// 本地数据库
var ItemObj = {
	createNew:function(id, img, title, cont){
		var g = {}
		g.id = id;
		g.img = img;
		g.title = title;
		g.cont = cont;
		return g;
	}
	,clone : function(other){
        var g = ItemObj.createNew(other.id, other.img, other.title, other.cont);
        return g;
    }
}
var ItemData = {
	// 数据源
	_lib:[]
	// 初始化
	, init:function(){}
	// 用服务器端的JSON数据创建本地库
	, createLib:function(jsonData){
		this._clearCache();
		var i, res, obj;
		for (i = 0; i < jsonData.length; i++) {
			res = jsonData[i];
			obj = ItemObj.createNew(res.id, res.img, res.title, res.digest);
			this._addLib(obj);
		}
	}
	// 添加一篇内容
	, appendData:function(obj){
		var newObj = ItemObj.clone(obj);
		this._addLib(newObj);
	}
	// 修改一片内容
	, updataData:function(obj){
		var res = this._lib;
		for (var i = 0; i < res.length; i++) {
			if (res[i].id === obj[i].id) {
				res[i] = ItemObj.clone(obj);
				return true;
			}
		}
		return false;
	}
	// 给ID返回id对象
	, getContById:function(int_id){
		for (var i = 0; i < this._lib.length; i++) {
			if (int_id === this._lib[i].id) {
				return this._lib[i];
			}
		}
		return undefined;
	}

	// 返回数据
	, getLib: function(){
		return this._lib;
	}
	// 返回数据长度用于暂时当作数据库返回ID
	, getLibLength:function(){
		return this._lib.length+1;
	}
	// 将数据添加到数据源
	, _addLib:function(arr){
		this._lib.push(arr);
		this._sort();
	}
	// 排序
	, _sort:function(){
		this._lib.sort(function(a, b){
			return b.id - a.id;
		})
	}
	, _clearCache: function() {
        this._lib.length = [];
    }
}

// 功能处理部分 CMain()

// 全局控制
var CMain = {
	// 临时文章Id
	_tempContId: null
	// 临时文章对象
	, _tempContObj:{}
	// 初始化
	, init:function(){
		// 获取服务器数据
		DATA.getData(function(res){
			ItemData.createLib(res);
			// 渲染显示页面数据
			CMain.renderDisplayData();
		});
	}
	/*-------临时接口-------*/
	// 临时文章Id
	, setTempContId:function(int_id){
		this._tempContId = int_id;
	}
	, getTempContId:function(){
		return this._tempContId;
	}
	// 临时文章对象
    , setTempContObj:function(obj){
        this._tempContObj = ItemObj.clone(obj);
    }
    , getTempContObj: function () {
        return this._tempContObj;
    }
    /*修改一条内容*/
    , makeUpdataCont:function(){
    	// 渲染显示页面数据
		CMain.renderDisplayData();
    }
    // 添加一条内容
    , makeAddCont:function(){
    	var newObj = this.getTempContObj();
    	// 将数据添加到数据源
    	ItemData.appendData(newObj);
    	
    	// 渲染显示页面数据
		CMain.renderDisplayData();
    }
    // 渲染显示页面数据
    ,renderDisplayData:function(){
		// 获取本地数据
		var newData = ItemData.getLib();
		// 渲染页面
		VMain.renderPanel(newData);
    }
}

// 页面显示部分 VMain()
var VMain = {
	// 初始化
	init:function(){
	}
	,renderPanel:function(res){
		// console.log(res);
		var doc = document;
		var html = '<a class="btn" onclick="VMain.onClickAddContPop()"><strong>+</strong> Add New</a>';
		for (var i = 0; i < res.length; i++) {
			html += '<div class="article" data-id="{0}"><img src="{1}" alt=""><h3 class="title">{2}</h3><p class="not">{3}</p><a class="edit">Edit</a></div>'.format(res[i].id, res[i].img, res[i].title, res[i].cont);
		}
		// 获取容器ID渲染页面
		doc.getElementById('content').innerHTML = html;

		// 获取编辑按钮
		var arr_div = doc.getElementsByClassName('article');
		var editBtn;
		for (var i = 0; i < arr_div.length; i++) {
			editBtn = arr_div[i].getElementsByClassName('edit')[0];
			editBtn.onclick = VMain.onClickUpdata;
		}
	}
	, onClickUpdata:function(){
		// 获取ID
		var dataId = parseInt(this.parentNode.getAttribute('data-id'));
		// console.log(dataId);
		// 记录点击的文章ID
		CMain.setTempContId(dataId);
		// 根据ID返回id对象
		var res = ItemData.getContById(dataId);

		var html = '<div id="form-article">'
				 + '<div class="form-group"><label>Image</label><input class="form-control" type="text" placeholder="http://" value="{0}"></div>'
				 + '<div class="form-group"><label>Title</label><input class="form-control" type="text" placeholder="title" value="{1}"></div>'
				 + '<div class="form-group"><label>Content</label><textarea class="form-control" rows="4" placeholder="content">{2}</textarea></div></div>';
			html = html.format(res.img, res.title, res.cont);

		xld.alert('Edit Article', html,'Cancel');
		$('.T-dialog').addClass('T-art');
		$('.T-foot').prepend('<a onclick="VMain.onClickUpdateCont()">Save</a>');

        // 阻止事件冒泡
        window.event.stopPropagation();
        window.event.cancelBubble = true;
	}
	,onClickUpdateCont:function(){
		// 获取弹框的信息
		var doc = document;
		var arr_div = doc.getElementById('form-article').getElementsByClassName('form-control');
		var str_img = arr_div[0].value.replace(/^\s*|\s*$|[&\|\\\*^%￥$#@\-]/g, '');
		var str_title= arr_div[1].value.replace(/^\s*|\s*$|[&\|\\\*^%￥$#@\-]/g, '');
		var str_cont = arr_div[2].value.replace(/^\s*|\s*$|[&\|\\\*^%￥$#@\-]/g, '');

		if (str_img == '') {
			alert('The image link cannot be empty!');
            return;
        }
        if(str_title == ''){
			alert('The title cannot be empty！');
            return;

		}
		if(str_cont == ''){
			alert('The content cannot be empty！');
            return;

		}

		var int_id = CMain.getTempContId();

		var obj = ItemData.getContById(int_id);
		console.log(obj)
		var newObj = obj;
		newObj.id = obj.id;
		newObj.img = str_img;
		newObj.title = str_title;
		newObj.cont = str_cont;
		
		console.log(newObj);
		CMain.setTempContObj(newObj);
		// 向服务器发送数据
		// CMain.makeUpdataCont(int_id, str_img, str_title, str_cont);
		CMain.makeUpdataCont();
		// 关闭修改弹框
        xld.delmodal();
	}
	,onClickAddContPop:function(){
		var html = '<div id="form-article">'
				 + '<div class="form-group"><label>Image</label><input class="form-control" type="text" placeholder="http://"></div>'
				 + '<div class="form-group"><label>Title</label><input class="form-control" type="text" placeholder="title"></div>'
				 + '<div class="form-group"><label>Content</label><textarea class="form-control" rows="4" placeholder="content"></textarea></div></div>';
		// 弹框
		xld.alert('Add Article', html,'Cancel');
		$('.T-dialog').addClass('T-art');
		$('.T-foot').prepend('<a onclick="VMain.onClickAddCont()">Save</a>');



	}
	,onClickAddCont:function(){
		var newid = ItemData.getLibLength();
		// console.log(newid)
		// 获取弹框的信息
		var doc = document;
		var arr_div = doc.getElementById('form-article').getElementsByClassName('form-control');
		var str_img = arr_div[0].value.replace(/^\s*|\s*$|[&\|\\\*^%￥$#@\-]/g, '');
		var str_title= arr_div[1].value.replace(/^\s*|\s*$|[&\|\\\*^%￥$#@\-]/g, '');
		var str_cont = arr_div[2].value.replace(/^\s*|\s*$|[&\|\\\*^%￥$#@\-]/g, '');

		if (str_img == '') {
			alert('The image link cannot be empty!');
            return;
        }
        if(str_title == ''){
			alert('The title cannot be empty！');
            return;

		}
		if(str_cont == ''){
			alert('The content cannot be empty！');
            return;

		}


		var newObj = CMain.getTempContObj();
		newObj.id = newid;
		newObj.img = str_img;
		newObj.title = str_title;
		newObj.cont = str_cont;
		// console.log(newObj);
		// 向服务器发送数据
		// CMain.makeAddCont(str_img, str_title, str_cont);
		CMain.makeAddCont();
		// 关闭修改弹框
        xld.delmodal();
	}
}


// 弹框控件 自己工作中的总结  用到了Jquery
var xld = {
	alert: function (title, msg, btn, func_ok) {
        xld.alert_ok = func_ok || xld.delmodal;
        var doc = document;
        var s = '<div class="T-modal" style="display:block;">'
            + '<div class="T-mask"></div>'
            + '<div class="T-dialog">'
            + '<div class="T-header"><p class="T-title">{0}</p></div>'
            + '<div class="T-body"><div class="T-content">{1}</div></div>'
            + (!btn ? '' : '<div class="T-foot"><a onclick="xld.alert_ok();">{0}</a></div>'.format(btn))
            + '</div></div>';
        s = s.format(title, msg);
        $('.T-modal').remove();
        $(doc.body).append(s);
        $('body').addClass('overflow');

        if ($('.T-modal .T-dialog input').length == 0) {
            $('.T-modal').on('click', function (event) {
                if ($(event.target || event.srcElement).hasClass('T-modal')) {
                    xld.delmodal();
                }
            });
        }
    }
    , delmodal: function () {
        $('.T-modal').hide();
        $('body').removeClass('overflow');
        setTimeout(function () {
            $('.T-modal').remove();
        }, 100);
    }
}


/*
 * 格式化字符串
 * 用法：'my name is {0}, age {1}'.format('xianzhi', 30)
 */
String.prototype.format = function() {
    var _this = this;
    for (var i = 0; i < arguments.length; i++) {
        _this = _this.replace(new RegExp('\\{' + i + '\\}', 'g'), arguments[i]);
    }
    return _this;
};








CMain.init()