var DATA = {
    getData: function(callback){
        var res = [
            {
                "id": 1,
                "title": "The Best JavaScript and CSS Libraries for 2017",
                "img": "https://tutorialzine.com/media/2017/12/the-best-javascript-and-css-libraries-for-2017.png",
                "digest": "Our mission at Tutorialzine is to keep you up to date with the latest and coolest trends in web development. Over the last year, we presented you a number of interesting libraries and resources, that we thought are worth checking out. That's why in this article we decided to share with you a collection of those, that stood out the most."
            },
            {
                "id": 2,
                "title": "The Best of CodePen for 2017",
                "img": "https://tutorialzine.com/media/2017/12/the-15-best-codepen-demos-2017.jpg",
                "digest": "With the new year around the corner, we thought it would be fun to look back and celebrate all the amazing projects submitted to CodePen. It is truly amazing what the web is capable of today, and how much can be achieved with just a browser and a tiny bit of HTML, CSS and JavaScript."
            }
        ];
        callback(res)
    }
};
